# Gnuplot script for generating the graphs for rmi-PID demo.
#
# Run this script in Gnuplot after having set filename by typing
# at gnuplot prompmt:
# gnuplot> filename="datafile"
# where datafile is the file containing the robot program output.
# Then, run this script: 
# gnuplot> load "PIDgraphs.gp"
#
# pf@ua.pt
# October 2015

set yrange [-35:35]
set multiplot layout 2,1
set title filename." Left motor"
plot filename using 1 with lines title 'SP', filename using 2 with lines title 'MV'
set title filename." Right motor"
plot filename using 3 with lines title 'SP', filename using 4 with lines title 'MV'
unset multiplot
pause -1 "Press Ctrl-C to end."