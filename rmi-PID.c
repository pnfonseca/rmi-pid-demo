#include "rmi-PID.h"

#define INTEGRAL_CLIP	100

PID_controller_t leftPID, rightPID;
unsigned int printVals=0;

void clip(float value, int limit);


void computePID(PID_controller_t * PID_controller_p){

	float prop_term;
	float deriv_term;
	float int_term;

	/* Proportional part */
	prop_term = PID_controller_p->Kp * PID_controller_p->inval;

	/* Derivative part */
	deriv_term = PID_controller_p->Kd*(PID_controller_p->inval - PID_controller_p->in_lastval);
	PID_controller_p->in_lastval = PID_controller_p->inval;

	/* Integral part */
	PID_controller_p->in_integral += PID_controller_p->inval;
	clip(PID_controller_p->in_integral,INTEGRAL_CLIP);

	int_term = PID_controller_p->Ki*PID_controller_p->in_integral;

	PID_controller_p->outval = prop_term + int_term + deriv_term;
}


void setPID(PID_controller_t * pid, float Kp, float Ki, float Kd){
	pid->Kd = Kd;
	pid->Ki = Ki;
	pid->Kp = Kp;
}

void getPID(PID_controller_t * pid, float* Kp_p, float* Ki_p, float* Kd_p){
	*Kd_p = pid->Kd;
	*Ki_p = pid->Ki;
	*Kp_p = pid->Kp;
}




void clip(float value, int limit)
{
	if(value > limit){
		value = limit;
	}
	if (value < -limit){
		value = -limit;
	}
}
