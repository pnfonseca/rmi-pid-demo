#
# Makefile for creating the PID demo for the DETI robot
#
# Usage:
# $> make clean; make KP=1 KI=0 KD=0 run
# to compile and load a program with Kp=1, Ki=0, Kd=0.
#
# The program will generate a data set at the output. Copy that 
# data set to a file (including the initial lines) and plot the
# result using gnuplot and the PIDgraph.gp script. (Consult this
# script for more instructions). 
# 
# PF, Nov 2012
# Revised:
# Date:        By:        	Description:
# March 2014   pf@ua.pt   	Changed to have more than one source file
# March 2014   fabiorico@ua.pt  Support for labE library
# Oct 2015		 pf@ua.pt      Demo for PID (RMI course)
#
# The default for the makefile is to compile all C files defined 
# in SOURCES and to create TARGET.hex, where TARGET is the name
# of the first file in SOURCES
#
# Other targets are:
#
# write     : write the latest version to PIC (and previously compile, 
#	     if necessary)
# run      : run pterm after loading the hex file.
# clean    : remove all unnecessary files
# cleanall : drastic cleaning; remove every output of compilation
#            including documentation
# doc      : run doxygen do create documentation
# help     : this help message...
#
# In the following line, define the name of your program source files
# by writing in the line starting with SOURCES= 
# (this is all you need to change...)
SOURCES=rmi-PIDexample.c rmi-PID.c

# Next line defines the USB port used by ldpic32. Change this if the
# USB port is not /dev/ttyUSB0
USBPORT=/dev/ttyUSB0

# Your program can have more than one source file. Use space to 
# separate the name of the source files. 
# The first file name will be used for the name of the hex file. 
#
# The name of the target can also be defined in the command line
# (overrides the definition in the file) when calling make.
#
# The following line shows how to compile a file called new_target.c
# $ make SOURCES=new_target.c
#
# ('$' stands for the command line prompt; it is not to be typed...)
#
# When you have more than one source file, include the file name list
# in "":
# $ make SOURCES="file1.c file2.c"
#
# or escape the space character with '\':
# $ make SOURCES=file1.c\ file2.c
#
# Same as before, but now write the resulting file into the PIC:
# $ make SOURCES=new_target.c write

#
# You can have a look at the rest of the file, to understand how
# a makefile works.
#
# Nevertheless, keep in mind:
# Don't change below this line, unless you know what you are doing!
#

#
# Global variables
# 
PICDIR=/opt/pic32mx
CFLAGS=-Wall -O2 -mprocessor=32MX795F512H -I$(CPATH)
# CFLAGS=-Wall -O2 -mprocessor=32MX795F512H 
CPATH=$(PICDIR)/include
CC=$(PICDIR)/bin/pic32-gcc
LIB=rmi-mr32.o
# TARGET is the first file in SOURCES
TARGET=$(basename $(firstword $(SOURCES)))

# The OBJECTS are same as .c files, but changing .c for .o
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))


#
# Compile source files 
#
%.o : %.c *.h
	$(CC) $(CFLAGS) -DKP=$(KP) -DKI=$(KI) -DKD=$(KD) -c -o $@ $<



#
# Create $(TARGET).hex
#
$(TARGET).hex : $(OBJECTS) $(LIB)
	$(PICDIR)/bin/pic32-ld $(OBJECTS) $(LIB) \
		-o $(TARGET).elf -L$(PICDIR)/lib -L$(PICDIR)/lib/gcc/pic32mx/3.4.4 \
		-ldetpic32 -lgcc -lm -lc -lsupc++ -lxcpp -Map $(TARGET).map
	$(PICDIR)/bin/pic32-bin2hex $(TARGET).elf



.PHONY: write run clean cleanall doc help all

all : $(TARGET).hex

#
# Write hex file to PIC
#
write : $(TARGET).hex
	ldpic32 -p $(USBPORT) -w $(TARGET).hex

#
# Run pterm at the end...
#
run : write
	pterm -p $(USBPORT)

#
# Housekeeping section...
#
clean : 
	echo "Cleaning all discardable files..."
	rm -rf *.elf *.map *~
	rm -f *.o

cleanall : clean
	echo "Cleaning all non-source files..."
	rm -rf *.hex *.o
	rm -rf Doc/
	rm -rf html/

doc : 
	doxygen

help:
	@cat Makefile | head -68 | less	

