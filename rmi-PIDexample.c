#include "rmi-mr32.h"
#include "rmi-PID.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>

const int VelSP=30;



int main(void)
{
	/* Initialize the robot */
	initPIC32();
	closedLoopControl( true );
	setVel2(0, 0);

	/* set PID parameters */
	/* These should be equal for both motors */
	setPID(&leftPID,  KP, KI, KD);
	setPID(&rightPID, KP, KI, KD);

	printf("# RMI PID example\n#\n");
	printf("# PID params: Kp=%f, Ki=%f, Kd=%f\n#\n",leftPID.Kp, leftPID.Ki, leftPID.Kd);

	printf("# Press start to continue\n");
	while(!startButton());
	printVals = 1;

	while(1)
	{
		setVel2(VelSP,-VelSP);
		wait(10);

		setVel2(-VelSP,VelSP);
		wait(10);
	}
	return 0;
}


