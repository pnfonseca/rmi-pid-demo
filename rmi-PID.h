/**
 * \file rmi-PID.h
 *
 * \brief Implementation of a PID controller
 *
 * \mainpage
 *
 * A C module that implements a PID controller.
 *
 * \author pf@ua.pt
 */

#define PID_DEMO
#define PRINT_VALS


/**
 * \brief PID controller structure
 *
 * Each PID controller corresponds to a PID_controller_t structure.
 */
typedef struct {
  int inval;			/**< Input value for PID controller (error) */
  int outval;			/**< Output value for PID (control signal) */
  float Kp;				/**< Kp: Proportional gain */
  float Kd;				/**< Kd: Derivative gain */
  float Ki;				/**< Ki: Integral gain */
  float in_integral;	/**< memory for the integral component */
  float in_lastval;		/**< memory for previous input value */
} PID_controller_t;

extern PID_controller_t leftPID;	/**< PID data structure for left motor */
extern PID_controller_t rightPID;	/**< PID data structure for right motor */

extern unsigned int printVals;

/**
 * \fn computePID(PID_controller_t * PID_contoller_p)
 *
 * \brief PID execution
 *
 * computePID performs the actions of each PID controller iteration:
 * reads the input and computes the corresponding output. It
 * should be called at every sampling instant.
 *
 * \param PID_controller_p		Pointer to the PID data structure
 */
void computePID(PID_controller_t * PID_contoller_p);

/**
 * \brief setter for a PID controller data structure
 *
 * \param PID_controller_p	Pointer to the PID controller data structure
 * \param Kp	Proportional gain
 * \param Ki	Integral gain
 * \param Kd	Derivative gain
 */
void setPID(PID_controller_t * PID_controller_p, float Kp, float Ki, float Kd);


/**
 * \brief getter for a PID controller data structure
 *
 * \param PID_controller_p	Pointer to the PID controller data structure
 * \param Kp_p	Proportional gain (pointer to)
 * \param Ki_p	Integral gain (pointer to)
 * \param Kd_p	Derivative gain (pointer to)
 */
void getPID(PID_controller_t * PID_controller_p, float* Kp_p, float* Ki_p, float* Kd_p);

